
function Brezli(svgId) {
    this.snap = Snap(svgId);

    this.name = name;
    this.knotRadius = 15;
    this.arrowSize = 6;
    this.arrowStrokeWidth = 2;
    this.strokeWidth = 1;

    this.nThreads = 0;

    this.createDefs();
}

Brezli.prototype.createId = function(id) {
    return this.snap.attr('id') + '_' + id;
}

Brezli.prototype.getId = function(id) {
    return '#' + this.createId(id);
}

Brezli.prototype.createDefs = function() {
    var arrowSize = this.arrowSize;
    var p = ['', '', '', ''];

    // right knot
    p[0] += 'M ' + (-arrowSize) + ' ' + (-arrowSize);
    p[0] += ' l ' + (2 * arrowSize) + ' ' + (2 * arrowSize);
    p[0] += ' m ' + (-arrowSize) + ' 0';
    p[0] += ' h ' + arrowSize;
    p[0] += ' v ' + (-arrowSize);

    // left knot
    p[1] += 'M ' + arrowSize + ' ' + (-arrowSize);
    p[1] += ' l ' + (-2 * arrowSize) + ' ' + (2 * arrowSize);
    p[1] += ' m ' + arrowSize + ' 0';
    p[1] += ' h ' + (-arrowSize);
    p[1] += ' v ' + (-arrowSize);

    // right-left knot
    p[2] += 'M ' + (-arrowSize / 2.) + ' ' + (- arrowSize);
    p[2] += ' l ' + arrowSize + ' ' + arrowSize;
    p[2] += ' l ' + (-arrowSize) + ' ' + arrowSize;
    p[2] += ' m ' + arrowSize + ' 0';
    p[2] += ' h ' + (-arrowSize);
    p[2] += ' v ' + (-arrowSize);

    // left-right knot
    p[3] += 'M ' + (arrowSize / 2.) + ' ' + (-arrowSize);
    p[3] += ' l ' + (-arrowSize) + ' ' + arrowSize;
    p[3] += ' l ' + arrowSize + ' ' + arrowSize;
    p[3] += ' m ' + (-arrowSize) + ' 0';
    p[3] += ' h ' + arrowSize;
    p[3] += ' v ' + (-arrowSize);

    var arrowBorder = [];
    var arrowStroke = [];
    var arrow = []

    for(i = 0; i < 4; ++i) {
        arrowBorder.push(this.snap.path(p[i]));
        arrowBorder[i].addClass('brezli_arrow brezli_arrowBorder');

        arrowStroke.push(this.snap.path(p[i]));
        arrowStroke[i].addClass('brezli_arrow brezli_arrowStroke');

        arrow.push(this.snap.group(arrowBorder[i], arrowStroke[i]));

        var id;
        switch(i) {
            case 0:
                id = 'arrowR';
                break;
            case 1:
                id = 'arrowL';
                break;
            case 2:
                id = 'arrowRL';
                break;
            default:
                id = 'arrowLR';
        }

        arrow[i].attr('id', this.createId(id));
        arrow[i].toDefs();
    }
}

function Knot(brezli, x, y, direction, noKnot, color) {
    this.center = [x, y];
    this.direction = direction;
    this.noKnot = false;
    this.knot = null;
    // TODO not used for now
    this.inStringL = null;
    this.inStringR = null;
    this.outStringL = null;
    this.outStringR = null;

    if(noKnot !== undefined) {
        this.noKnot = noKnot;
    }

    // TODO remove
    this.color = 'white';
    if(color !== undefined) {
        this.color = color;
    }
}

Knot.prototype.setColor = function(color) {
    this.color = color;
}

Knot.prototype.draw = function() {
    if(this.noKnot) {
        return;
    }

    var circle = brezli.snap.circle(this.center[0], this.center[1], brezli.knotRadius);
    circle.addClass('brezli_knot');
    circle.attr({
        fill: this.color
    });

    var arrowId;
    switch(this.direction) {
        case 'r':
            arrowId = 'arrowR';
            break;
        case 'l':
            arrowId = 'arrowL';
            break;
        case 'rl':
            arrowId = 'arrowRL';
            break;
        default:
            arrowId = 'arrowLR';
    }

    arrow = brezli.snap.select(brezli.getId(arrowId)).use();
    arrow.transform('t ' + this.center[0] + ' ' + this.center[1]);
    brezli.snap.append(arrow);

    this.knot = brezli.snap.group(circle, arrow);

    this.knot.data('matrixOver', new Snap.Matrix());
    this.knot.data('matrixOver').scale(1.2, 1.2, this.center[0], this.center[1]);
    this.knot.data('matrixOut', new Snap.Matrix());
    this.knot.data('matrixOut').scale(1., 1., this.center[0], this.center[1]);

    this.knot.hover(this.animateOver.bind(this), this.animateOut.bind(this));

}

Knot.prototype.animateOver = function() {
    this.knot.animate({transform: this.knot.data('matrixOver')}, 100);
}

Knot.prototype.animateOut = function() {
    this.knot.animate({transform: this.knot.data('matrixOut')}, 100);
}

function Thread(number, x, y, color) {
    this.number = number;
    this.color = 'white';
    this.startCoordinates = [x, y];
    this.path = null;
    this.thread = null;

    if(color !== undefined) {
        this.color = color
    }
}

Thread.prototype.setPath = function(path) {
    this.path = path;
}

Thread.prototype.setColor = function(color) {
    this.color = color;
}

Thread.prototype.getColor = function() {
    return this.color;
}

Thread.prototype.draw = function() {
    var lineBorder = brezli.snap.path(this.path);
    lineBorder.addClass('brezli_thread brezli_threadBorder');
    var lineStroke = brezli.snap.path(this.path);
    lineStroke.addClass('brezli_thread brezli_threadStroke');
    lineStroke.attr({
        stroke: this.color
    });
    this.thread = brezli.snap.group(lineBorder, lineStroke);
    this.thread.hover(this.animateOver.bind(this), this.animateOut.bind(this));
}

Thread.prototype.animateOver = function() {
    // TODO don't use hardcoded values
    this.thread[0].animate({'stroke-width': 1.2 * (8 + 2 * 1)}, 100);
    this.thread[1].animate({'stroke-width': 1.2 * 8}, 100);
}

Thread.prototype.animateOut = function() {
    this.thread[0].animate({'stroke-width': 8 + 2 * 1}, 100);
    this.thread[1].animate({'stroke-width': 8}, 100);
}

function BrezliFromTxt(brezli, txt) {
    brezli.snap.clear();
    brezli.createDefs();

    var txtSplitted = txt.split('\n');
    // remove last element if it's an empy string
    if(txtSplitted[txtSplitted.length-1] == '') {
        txtSplitted.pop();
    }
    for(var i = 0; i < txtSplitted.length; ++i) {
        txtSplitted[i] = txtSplitted[i].split(',');
    }

    var d = brezli.knotRadius;

    var origin = [2*d, d];
    var knots = [];

    // TODO check number of of rows
    if(txtSplitted[0].length == txtSplitted[1].length) {
        brezli.nThreads = 2 * txtSplitted[0].length + 1;
    } else {
        brezli.nThreads = 2 * txtSplitted[0].length;
    }

    var threadsEven = brezli.nThreads % 2 == 0;

    for(row = 0; row < txtSplitted.length; ++row) {
        knots.push([]);
        var rowEven = row % 2 == 0;
        var nKnots = 0;
        var y = origin[1] + (2 + 2*row)*d;
        if(!threadsEven) {
            nKnots = Math.ceil(brezli.nThreads / 2);
        } else {
            nKnots = brezli.nThreads / 2;
            if(!rowEven) {
                ++nKnots;
            }
        }

        for(var knot = 0; knot < nKnots; ++knot) {
            var x = origin[0] + (1 + 4*knot)*d;
            var direction;
            var noKnot = false;
            if(threadsEven) {
                if(rowEven) {
                    direction = txtSplitted[row][knot];
                } else {
                    x -= 2*d;
                    // leftmost thread
                    if(knot == 0) {
                        noKnot = true;
                        direction = 'lr';
                    // rightmost thread
                    } else if(knot == (nKnots-1)) {
                        noKnot = true;
                        direction = 'rl';
                    }
                    else {
                        direction = txtSplitted[row][knot-1];
                    }
                }
            } else {
                if(rowEven) {
                    if(knot < (nKnots-1)) {
                        direction = txtSplitted[row][knot];
                    // rightmost thread
                    } else {
                        noKnot = true;
                        direction = 'rl';
                    }
                } else {
                    x -= 2*d;
                    if(knot > 0) {
                        direction = txtSplitted[row][knot-1];
                    // leftmost thread
                    } else {
                        noKnot = true;
                        direction = 'lr';
                    }
                }
            }
            knots[row].push(new Knot(brezli, x, y, direction, noKnot));
        }
    }

    var threads = [];

    for(var thread = 0; thread < brezli.nThreads; ++thread) {
        var x = origin[0] + 2*thread*d;
        var y = origin[1];
        threads.push(new Thread(thread, x, y));
        threads[thread].setColor(getColor(thread));
        var p = 'M ' + x + ' ' + y;
        p += ' v ' + d;

        var threadNumber = thread;

        for(row = 0; row < txtSplitted.length; ++row) {
            var rowEven = row % 2 == 0;
            var knotColumn = (rowEven) ? Math.floor(threadNumber / 2) : Math.floor((threadNumber + 1) / 2);
            var colorize = !knots[row][knotColumn].noKnot;
            var knotDirection = knots[row][knotColumn].direction;
            var nextInLeft = (rowEven) ? threadNumber % 2 == 0 : threadNumber % 2 == 1;

            var move;

            if(nextInLeft) {
                switch(knotDirection) {
                    case 'r':
                        move = 'r';
                        ++threadNumber;
                        break;
                    case 'l':
                        move = 'r';
                        colorize = false;
                        ++threadNumber;
                        break;
                    case 'rl':
                        move = 'rl';
                        break;
                    default:
                        move = 'rl';
                        colorize = false;
                }
            } else {
                switch(knotDirection) {
                    case 'r':
                        move = 'l';
                        colorize = false;
                        --threadNumber;
                        break;
                    case 'l':
                        move = 'l';
                        --threadNumber;
                        break;
                    case 'rl':
                        move = 'lr';
                        colorize = false;
                        break;
                    default:
                        move = 'lr';
                }
            }

            if(colorize) {
                knots[row][knotColumn].setColor(threads[thread].getColor());
            }

            switch(move) {
                case 'r':
                    p += ' l ' + (2 * d) + ' ' + (2 * d);
                    break;
                case 'l':
                    p += ' l ' + (-2 * d) + ' ' + (2 * d);
                    break;
                case 'rl':
                    p += ' l ' + d + ' ' + d + ' l ' + (-d) + ' ' + d;
                    break;
                default:
                    p += ' l ' + (-d) + ' ' + d + ' l ' + d + ' ' + d;
                    break;
            }
        }

        threads[thread].setPath(p);
        threads[thread].draw();
    }

    for(var row = 0; row < knots.length; ++row) {
        for(var column = 0; column < knots[row].length; ++column) {
            knots[row][column].draw();
        }
    }
}

function getColor(i) {
    var colors = ['brown', 'yellow', 'cornflowerblue', 'forestgreen', 'grey', 'mediumvioletred ', 'orange', 'tan', 'thistle', 'navy'];
    return colors[i % colors.length];
}
